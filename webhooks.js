var colors = require('colors');
var db = require('./db.js');

module.exports = function (app, addon) {

    // Atlassian Connect Express fires events when a webhook is received, in order to authenticate the webhook for you.
    addon.descriptor.modules.webhooks.forEach(function (webhook) {
        addon.on(webhook.event, function (evt, body, req) {
            var d = new Date;
            var confluenceEvent = {};
            confluenceEvent.action = evt;
            confluenceEvent.user = body.user;
            confluenceEvent.spaceKey = body.page.spaceKey;
            confluenceEvent.pageId = body.page.id;
            confluenceEvent.pageTitle = body.page.title;
            confluenceEvent.pageCreationDate = body.page.creationDate;

            db.confluence.create(confluenceEvent);
            console.log();
            console.log(evt.bold.yellow.inverse);
            console.log(require('util').inspect(body, {
                colors: true,
                depth: 6,
                showHidden: true,
            }));
        });
    });


    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });


    app.get('/installed', function (req, res) {
        res.status(200);
    });
};
